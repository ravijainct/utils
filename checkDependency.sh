#!/bin/bash

set -e

dependenciesFile="dependencies.txt"
invalidLibrariesFile="invalidLibraries.txt"

./gradlew dependencies | sed 's/ //g' |  sed 's/|//g' | sed 's/+---//g' | sed 's/\\---//g' > $dependenciesFile

invalidLibraries=($(grep -f $invalidLibrariesFile $dependenciesFile | sort --unique))
for invalidLibrary in "${invalidLibraries[@]}"; do
  echo "Fix required for: $invalidLibrary"
done

if [[ ${#invalidLibraries[@]} -gt 0 ]]; then
  echo "Pleease Update/Remove the dependencies mentioned above before proceeding for deployment."
  echo "${#invalidLibraries[@]} dependencies require modification."
  exit 1
else
  echo "Gradle dependency check successful."
fi